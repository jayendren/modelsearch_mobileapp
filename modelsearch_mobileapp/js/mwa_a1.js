//
// code reuse: 
// dynamic listview (info page and details page) generated from json file 
// Ref: http://jsfiddle.net/hungerpain/52Haa/
//
// main page //
//
// code reuse: pagecreate event for first page
// Ref: https://api.jquerymobile.com/pagecreate/
$(document).on("pagecreate", "#main-page", function () {
    // mock json data
    // in production/live feed this will point to a URL
    // json endpoint
    var endpoint = "json/mwa_a1.json"; 
    // string for adding <li/>
    var li       = "";
    // code reuse:
    // Ref: http://api.jquery.com/jquery.getjson/  
    $(document).ready(function(){
        $.ajax({
            type:     "GET",
            url:      endpoint,
            dataType: "JSON",
            // in production/live feed this should include an api-key, e.g:
            //
            //   data: {
            //      'api-key': '11111111111111111'
            //   },  
            // and use a dataFilter function to sanitize the response 
            // Ref: 
            // http://api.jquery.com/jquery.ajax/
            // https://stackoverflow.com/questions/39217240/using-datafilter-from-jquery-ajax-with-json-data
            //
            // code reuse: async: true
            // Ref: http://api.jquery.com/jquery.ajax/      
            async:    true,
            success:  jsonParser,
            error:    jsonFailed            
        });
    });
    function jsonParser(data) {
        var info = data;
        // container for $li to be added
        $.each(info, function (i, name) {
            var username = name.name;
            var photo_url = name.photo_url;
            // code reuse: jqm listview thumbnails
            // Ref: http://demos.jquerymobile.com/1.4.3/listview/               
            li += '<li><a href="#" id="' + 
            i + '" class="info-go" data-transition="pop">' + 
                username + 
                '<img src="' +
                photo_url +             
                '"></a></li>'
            ;
        });
        //
        // code reuse: wait for append to finish
        // Ref: https://api.jquery.com/promise/        
        // append username + image collected from json to list to ul on main-page
        $("#prof-list").append(li).promise().done(function () {
            // code reuse: add the click event for the redirection to happen to #details-page
            // Ref: https://www.w3schools.com/jquery/event_preventdefault.asp
            $(this).on("click", ".info-go", function (e) {
                e.preventDefault();
                //
                // code reuse: store the information in the next page's data 
                // Ref: https://api.jquery.com/data/
                $("#details-page").data("info", info[this.id]);
                //
                // code reuse: change to details page with transition
                // Ref: https://api.jquerymobile.com/1.3/jQuery.mobile.changePage/
                $.mobile.changePage("#details-page", {
                    transition: "pop",
                });
            });
            // code reuse: refresh list to enhance its styling.
            // Ref: https://api.jquerymobile.com/listview/#method-refresh
            $(this).listview("refresh");
        });
    }
    function jsonFailed(jqXHR, textStatus, errorThrown) {
        $("#prof-list").append('<li>' + textStatus + errorThrown + ': unable to load file.</li>');
    }    
});
//
// details page //
// // code reuse: trigger to page before transition animation is kicked off.
// Ref: https://api.jquerymobile.com/pagebeforeshow/
$(document).on("pagebeforeshow", "#details-page", function () {
    //
    // code reuse: get the data that was stored for details page
    // Ref: https://api.jquery.com/data/    
    var info               = $(this).data("info");
    // vars to insert into html 
    var info_view          = "";
    var interests_view     = "";
    var photo_gallery_view = "";
    // collect data from json for display in the details-page
    var id             = info.id;
    var age            = info.age;
    var name           = info.name;
    var gender         = info.gender;
    var agency         = info.agency;
    var email          = info.email;
    var phone          = info.phone;
    var country        = info.country;
    var photo_url      = info.photo_url;
    var bio            = info.bio;
    // code reuse: iterate through object keys and values
    // Ref: https://learn.jquery.com/using-jquery-core/iterating/
    // https://stackoverflow.com/questions/23899543/how-do-i-use-jquery-to-iterate-through-nested-json-in-a-single-pass
    //
    // interests feed
    $.each(info.interests, function(key, value){
       var interestName  = this.name;
       var interestNotes = this.notes;
        interests_view   = '<p> Interested in ' + 
            interestName + 
            ' and ' +  
            interestNotes +
            '. </p>'
        ;           
    })
    // photo gallery feed
    // code reuse: swipebox alternative lightbox 
    // Ref: http://brutaldesign.github.io/swipebox/
    var info                  = $(this).data("info");
    $.each(info.photo_gallery, function(key, value){
       var large              = this.large;
       var thumbnail          = this.thumbnail;
        photo_gallery_view    += '<p><a href="' +
            large +
            '" class="swipebox"' +
            ' title="' +
            name +
            '">' +
            '<img src="' +
            thumbnail +
            '" alt="image">' +
            '</a></p>'
        ;   
    })    
    //
    // build the html that will be added to the details-page html
    //
    info_view = '<center><img class="profile_img" src="' +
        photo_url +
        '">' +
        '<h3>About</h3>' +
        '<p>'+ 
        name +
        ' (' +
        age + 
        ' years old) ' +        
        'works for ' + 
        agency +
        ' and hails from ' +
        country +
        '</p>' +
        interests_view +
        '<h3>Biography</h3>' +
        '<p>' + 
        bio +
        '</p>' +
        '<h3>Contact Details</h3>' +
        '<p>' + 
        email +
        '</p>' +  
        '<h3>Gallery</h3>' +
        photo_gallery_view +
        '</center>'
    ;
    // code reuse: add info_view to html
    // Ref: https://api.jquery.com/find/
    $(this).find("[data-role=content]").html(info_view);
});