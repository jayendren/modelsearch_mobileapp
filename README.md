### Table of Contents

- [About](#about)
- [Requirements](#requirements)
  * [JQuery Mobile](#jquery-mobile)
  * [Web Fonts](#web-fonts)
  * [Web Server](#web-server)
- [Directory layout](#directory-layout)
- [Deployment instructions](#deployment-instructions)
  * [webserver](#webserver)
    + [httpd/apache](#httpd-apache)
    + [nginx](#nginx)
- [Searching for Models](#searching-for-models)
- [Viewing Models Profile](#viewing-models-profile)
- [Viewing Models Gallery](#viewing-models-gallery)
- [Help](#help)
- [About](#about-1)
- [Updating mock data in json file](#updating-mock-data-in-json-file)

### About


Application geared towards advertising agencies that want to hire models for advertisments.
Search items are generated via a json feed.
The data is hardcoded and found in the json/mwa_a1.json file
	            

![](modelsearch_mobileapp/img/help/0_home.png)

### Requirements

#### JQuery Mobile

* Uses an offline version of jqm - https://jquerymobile.com/download/

#### Web Fonts

* Uses an offline version of fontawesome - https://fontawesome.com/get-started
* Uses an online version of OpenSans - https://fonts.googleapis.com/css?family=Open+Sans

#### Web Server

* nginx or httpd web server

### Directory layout
```
modelsearch_mobileapp # www_root
├── README.md
├── css
│   ├── font-awesome.min.css
│   ├── images
│   ├── jquery.mobile-1.4.5.min.css
│   ├── mwa_a1.css
│   ├── swipebox.css
│   └── themes
├── fonts
│   ├── FontAwesome.otf
│   ├── fontawesome-webfont.eot
│   ├── fontawesome-webfont.svg
│   ├── fontawesome-webfont.ttf
│   ├── fontawesome-webfont.woff
│   ├── fontawesome-webfont.woff2
│   ├── glyphicons-halflings-regular.eot
│   ├── glyphicons-halflings-regular.svg
│   ├── glyphicons-halflings-regular.ttf
│   ├── glyphicons-halflings-regular.woff
│   └── glyphicons-halflings-regular.woff2
├── img
│   ├── help
│   └── models
├── index.html
├── js
│   ├── jquery-1.11.1.min.js
│   ├── jquery.mobile-1.4.5.min.js
│   ├── jquery.swipebox.js
│   └── mwa_a1.js
├── json
│   └── mwa_a1.json
└── notes.txt

9 directories, 23 files
```
### Deployment instructions

* the modelsearch_mobileapp (www root directory) needs to be placed in the web root of the webserver


#### webserver

##### httpd/apache

* place the modelsearch_mobileapp in the defined DocumentRoot directive - https://httpd.apache.org/docs/2.4/mod/core.html#documentroot
* e.g: ```/var/www/sites``` in unix based systems, or ```c:\www\sites``` in windows based systems

##### nginx

* place the modelsearch_mobileapp in the defined root directive - http://nginx.org/en/docs/http/ngx_http_core_module.html#root
* e.g: ```/var/www/sites``` in unix based systems, or ```c:\www\sites``` in windows based systems 


### Searching for Models

* In the "Search for Models..." input box, type a name of a model.
![](modelsearch_mobileapp/img/help/1_search.png)
        
### Viewing Models Profile

* Search for a Model, then tap on the '>' button on the right.
![](modelsearch_mobileapp/img/help/2_profile.png)

### Viewing Models Gallery

* In the Models Profile Page, view the Gallery at the end of the page.
* Tap on any of the thumbnails to view them in fullscreen.
* Slide left or right to go back/forward in the Gallery.

### Help

* Access the Help by clicking on the Help icon on the left and bottom navbars
![](modelsearch_mobileapp/img/help/4_help.png)

### About

* Access the Help by clicking on the About icon on the left and bottom navbars
![](modelsearch_mobileapp/img/help/5_about.png)

### Updating mock data in json file

* Edit the json/mwa_a1.json file, and append content to the end of the file:
 
```
...
},                
    {
        "id": 7,
        "age": 23,
        "name": "Cindy Mace",
        "gender": "female",
        "agency": "Sky Models",
        "email": "Cindy.Mace@sky-models.com",
        "country": "United States",                        
        "bio": "I’m a amateur model ready to get my foot in the door.",
        "interests": [
            {
                "name": "Abstract Art",
                "notes": "enjoys spending Sundays painting"
            }
        ],
        "photo_url": "img/models/Cindy_profile.jpg",
        "photo_gallery": [
            {
                "large": "img/models/Cindy_large_1.jpg",
                "thumbnail": "img/models/Cindy_thumbnail_1.jpg"
            },
            {
                "large": "img/models/Cindy_large_2.jpg",
                "thumbnail": "img/models/Cindy_thumbnail_2.jpg"
            },
            {
                "large": "img/models/Cindy_large_3.jpg",
                "thumbnail": "img/models/Cindy_thumbnail_3.jpg"
            }                                               
        ]           
    }
] 
```